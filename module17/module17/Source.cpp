#include <iostream>
#include <random>
#include <ctime>
#include <iomanip>

const int arr_length = 10;

class Vector {
private:
	double x;
	double y;
	double z;

	double getFunctionLengthVector() {
		return std::sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}
public:
	Vector() : x(0), y(0), z(0) {}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {}

	void ShowVector() {
		std::cout << x << " " << y << " " << z << " " << std::endl;
	}

	void printVectorLength() {
		double vecLength;
		vecLength = std::sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
		std::cout << "(Alternative) Length this vector is " << std::sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2)) << std::endl;
		std::cout << "Length this vector is " << vecLength << std::endl;
		std::cout << "Length in private block this vector is " << getFunctionLengthVector() << std::endl << std::endl;
	}
};

class myUselessClass {
private:
	int arr_int[arr_length] = { 11,22,33,44,55,66,77,88,99,111 };
	float arr_float[arr_length] = { 11.f,22.f,33.f,44.f,55.f,66.f,77.f,88.f,99.f,111.f };
	double arr_double[arr_length] = { 11.11,22.22,33.33,44.44,55.55,66.66,77.77,88.88,99.99, 111.111 };
	char arr_char[arr_length] = { 'U','M','N','B','V','C','Q','W','E','R' };
	bool arr_bool[arr_length] = { false,true,false,true,false,true,false,true,false,true };

	int integertype = arr_int[rand() % arr_length];
	float floattype = arr_float[rand() % arr_length];
	double doubletype = arr_double[rand() % arr_length];
	char chartype = arr_char[rand() % arr_length];
	bool booltype = arr_bool[rand() % arr_length];
public:
	myUselessClass() : integertype(arr_int[rand() % 10]), 
		floattype(arr_float[rand() % 10]), 
		doubletype(arr_double[rand() % 10]),
		chartype(arr_char[rand() % 10]), 
		booltype(arr_bool[rand() % 10]) {}
	myUselessClass(
		int _integertype,
		float _floattype,
		double _doubletype,
		char _chartype,
		bool _booltype) : 
		integertype(_integertype),
		floattype(_floattype),
		doubletype(_doubletype),
		chartype(_chartype),
		booltype(_booltype) {}

	void ShowMyClass() {
		std::cout << "Class: " << integertype << " " << floattype << " " << doubletype << " " << chartype << " " << booltype << " " << std::endl;
	}

	auto SumAllInMyClass() {
		return int(integertype + floattype + doubletype + chartype + booltype);
	}
};

int main() {
	srand((unsigned)time(0));
	std::cout << std::setprecision(10);
	Vector v1;
	Vector v2(10, 10, 10);
	//v1
	v1.ShowVector();
	v1.printVectorLength();
	//v2
	v2.ShowVector();
	v2.printVectorLength();
	myUselessClass variable1;
	myUselessClass variable2(99, 0.111f, 0.11111111, 'T', true);
	variable1.ShowMyClass();
	std::cout << variable1.SumAllInMyClass() << std::endl;
	variable2.ShowMyClass();
	std::cout << variable2.SumAllInMyClass() << std::endl;
}